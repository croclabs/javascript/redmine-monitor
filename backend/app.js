require('dotenv').config();

let createError = require('http-errors');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');

let indexRouter = require('./routes/index');
let usersRouter = require('./routes/users');
let authRouter = require('./routes/auth');

let bodyParser = require("body-parser");

let app = express();
let session = require('express-session');

app.set('trust proxy', 1);
app.use(
  session(
    {
      secret: process.env.SESSION_KEY, // set a real and safe key value in .env file
      resave: false,
      saveUninitialized: true,
      cookie: {
        secure: false // in https set to true
      }
    }
  )
);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(bodyParser.json());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

if (process.env.LOGIN_NEEDED === "true") {
  app.use(function(req, res, next) {
    if (req.url.startsWith('/login') ||
        req.url.startsWith('/auth/user') ||
        req.url.startsWith('/auth/login') ) {
      next();
    } else if (!req.session.user) {
      console.warn('no user found --> login');
      res.status(403);
      res.end();
    } else {
      console.warn('All good, showing page');
      next();
    }
  });
}

app.use('/', indexRouter);
app.use('/issues', usersRouter);
app.use('/auth', authRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
