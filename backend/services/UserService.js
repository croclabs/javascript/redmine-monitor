class UserService {
    static login(username, password) {
        if (!username || !password) {
            return null;
        }

        return {username: username, loggedIn: true};
    }
}

module.exports = UserService;