import React, { Component } from 'react';
import Center from '../design/Center';
import Full from '../design/Full';
import { Button, TextField } from '@mui/material';
import FormCard from '../forms/FormCard';
import FormComponent from '../forms/FormComponent';
import LoginService from '../../services/LoginService';

function login() {
    LoginService.login();
}

class Login extends Component {
    render() {
        return (
            <Full>
                <Center>
                    <FormCard marginAuto>
                        <FormComponent>
                            <h1 style={{marginTop: '0', textAlign: 'center'}}>Login</h1>
                        </FormComponent>

                        <FormComponent>
                            <TextField
                                id="username"
                                label="Username"
                            />
                        </FormComponent>
                        <FormComponent>
                            <TextField
                                id="password"
                                label="Password"
                                type="password"
                                autoComplete="current-password"
                            />
                        </FormComponent>
                        <FormComponent>
                            <Button sx={{width: '100%', marginTop: '20px'}} variant="contained" onClick={login}>Login</Button>
                        </FormComponent>
                    </FormCard>
                </Center>
            </Full>
        );
    }
}

export default Login;