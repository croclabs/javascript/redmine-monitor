import React, { Component } from 'react';
import Box from '@mui/material/Box';

class Center extends Component {
    render() {
        return (
            <Box sx={{margin: 'auto', height: '100%', display: 'flex'}}>
                {this.props.children}
            </Box>
        );
    }
}

export default Center;