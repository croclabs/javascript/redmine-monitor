import React, { Component } from 'react';
import Box from '@mui/material/Box';

class Full extends Component {
    render() {
        return (
            <Box sx={{height: '100vh', width: '100vw'}}>
                {this.props.children}
            </Box>
        );
    }
}

export default Full;