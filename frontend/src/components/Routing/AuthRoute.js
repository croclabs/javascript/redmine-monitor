import React, { Component } from 'react';
import Login from '../login/Login';

class AuthRoute extends Component {
    state = {user: {username: '', loggedIn: false}, attempted: false};

    componentDidMount() {
        fetch('/auth/user')
            .then(res => {
                return res.json()
            })
            .then(user => {
                console.log('From Res');
                console.log(user);
                this.setState({user: user, attempted: true})
            });
    }

    render() {
        let content;

        console.log(this.state.user);

        if (this.state.user.loggedIn) {
            console.warn("User: " + !this.state.user)
            console.warn('Show page 1');
            content = this.props.children;
        } else if (!this.state.user.loggedIn && !this.state.attempted) {
            content = null;
        } else {
            content = <Login />;
        }

        return (
            content
        );
    }
}

export default AuthRoute;