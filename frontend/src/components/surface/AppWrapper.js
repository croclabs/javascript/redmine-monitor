import React, { Component } from 'react';
import Box from '@mui/material/Box';

class AppWrapper extends Component {
    render() {
        return (
            <Box sx={{ margin: '10px', width: '100%', height: '100%'}}>
                {this.props.children}
            </Box>
        );
    }
}

export default AppWrapper;