import React, { Component } from "react";
import Chart from "react-apexcharts";
import Card from '@mui/material/Card';

export class RadialChart extends Component {
    constructor(props) {
        super(props);

        this.state = {

            series: [44, 55, 67, 83],
            options: {
                chart: {
                    id: 'radialBar',
                    toolbar: {
                        show: true,
                        tools: {
                          download: true,
                        },
                    }
                },
                plotOptions: {
                    radialBar: {
                        dataLabels: {
                            name: {
                                fontSize: '22px',
                            },
                            value: {
                                fontSize: '16px',
                            },
                            total: {
                                show: true,
                                label: 'Total',
                                formatter: function (w) {
                                    // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
                                    return 249
                                }
                            }
                        }
                    }
                },
                labels: ['Apples', 'Oranges', 'Bananas', 'Berries'],
            },


        };
    }

    render() {
        return (
            <Card sx={{ margin: '10px', padding: '10px', display: 'inline-block'}}>
                <Chart
                    options={this.state.options}
                    series={this.state.series}
                    type="radialBar"
                    height={350}
                />
            </Card>
        );
    }
}