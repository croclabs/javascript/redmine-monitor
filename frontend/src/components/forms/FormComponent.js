import { Box } from '@mui/material';
import React, { Component } from 'react';

class FormComponent extends Component {
    render() {
        return (
            <Box sx={{ 
                marginTop: '5px',
                marginBottom: '5px'
            }}>
                {this.props.children}
            </Box>
        );
    }
}

export default FormComponent;