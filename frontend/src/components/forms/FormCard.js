import React, { Component } from 'react';
import Card from '@mui/material/Card';

class FormCard extends Component {
    render() {
        return (
            <Card sx={{ 
                margin: this.props.marginAuto ? 'auto' : '0',
                padding: '20px',
                display: 'inline-flex',
                flexDirection: 'column'
            }}>
                {this.props.children}
            </Card>
        );
    }
}

export default FormCard;