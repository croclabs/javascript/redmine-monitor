import React, { Component } from 'react';
import './App.css';
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/AccountCircle';
import { BarChart } from './components/charts/BarChart';
import MenuAppBar from './components/surface/AppBar';
import { RadialChart } from './components/charts/RadialCart';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Login from './components/login/Login';
import AuthRoute from './components/Routing/AuthRoute';

class App extends Component {
  state = {
    issues: {
      issues: []
    }
  };

  constructor(props) {
    super(props);
    this.fetch = this.fetch.bind(this);
}

  componentDidMount() { 
    this.fetch();
  }

  fetch() {
    fetch('/issues')
      .then(res => res.json())
      .then(issues => this.setState({ issues }));
  }

  render() {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="/login" element={<Login></Login>}/>
          <Route path="/" element={
            <AuthRoute>
              <div>
                <div id="menu">
                  <MenuAppBar />
                </div>
                <div id="content" style={{overflowY: 'auto', maxHeight: 'calc(100vh - 64px)'}}>
                  <BarChart/>
                  <RadialChart/>
                  <Box sx={{ margin: '10px', idth: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                    <nav aria-label="main mailbox folders">
                      <List>
                        {this.state.issues.issues.map(issue =>
                          <ListItem key={issue.id} disablePadding>
                            <ListItemButton onClick={this.fetch}>
                              <ListItemIcon>
                                <InboxIcon />
                              </ListItemIcon>
                              <ListItemText primary={issue.subject} />
                            </ListItemButton>
                          </ListItem>
                        )}
                      </List>
                    </nav>
                  </Box>
                </div>
              </div>
            </AuthRoute>
          }/>
        </Routes>
      </BrowserRouter>
    );
  }
}

export default App;